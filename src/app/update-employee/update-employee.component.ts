import { NgForOf } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router} from '@angular/router';
import { ApiserviceService } from '../apiservice.service';
import { Employee } from '../employee';
@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {
  @ViewChild('f') validateForm = NgForm;
  id:any;
  employee:Employee=new Employee();


  constructor( private apiservice:ApiserviceService,
    private route :ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.id=this.route.snapshot.params['id'];
    this.apiservice.getEmployeeById(this.id).subscribe( data =>{
      this.employee=data
      console.log(data);
    },
      
    
    error => console.log(error));
  }
  OnSubmit(){
      this.apiservice.updateEmployee(this.id,this.employee).subscribe( data =>{
        this.goToEmployeeList();
      })
   }
   goToEmployeeList(){
    this.router.navigate(['/employee-list']);
  }
}
