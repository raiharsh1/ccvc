import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ApiserviceService } from '../apiservice.service';
import { Employee } from '../employee';
import { NgForm} from '@angular/forms'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  @ViewChild('f') validateForm =NgForm;

  employee: Employee = new Employee();
  constructor(private apiservice:ApiserviceService,private toastr: ToastrService,
              private router:Router) { }

  ngOnInit(): void {
  }
          saveEmployee(){
            this.apiservice.creatEmployee(this.employee).subscribe(data =>{
                    console.log(data);
                    this.goToemployeeList();
                    this.toastr.success('Save Successfully');
            },
            error => console.log(error));
          }

          goToemployeeList(){
                        this.router.navigate(['/employee-list'])
          }

OnSubmit(){
  console.log(this.employee);
  this.saveEmployee();
}
}
