import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee';
import { ApiserviceService } from '../apiservice.service'; 
import { subscribeOn } from 'rxjs';
import { Router  } from '@angular/router';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employees : Employee[]=[];
  getemployees:any;
  constructor(private employeedata:ApiserviceService,private router:Router) 
  {
   }

  ngOnInit(): void { 

    this.getEmp();
  }
  getEmp(){
    this.employeedata.getemployees().subscribe((data: any)=>{
      this.getemployees=data;
      console.warn(this.getemployees)
    })
  }
  
  
  deleteEmployee(id: any){
    this.employeedata.deleteEmployee(id).subscribe((id: any)=>{
      if(id != null || id != ''){
        this.getEmp();
      }
    });
    }

    updateEmployee(id:any){
      this.router.navigate(['/updateEmployee',id]);
    }

    employeeDetails(id:any){
      this.router.navigate(['/employeeDetails',id]);
    }
  }


