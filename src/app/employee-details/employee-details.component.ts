import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiserviceService } from '../apiservice.service';
import { Employee } from '../employee';
import { Router } from '@angular/router';


@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {



  id:any
  employee :any
  constructor(private route: ActivatedRoute,private apiservice:ApiserviceService, private router: Router) { }

  ngOnInit(): void {
    this.id=this.route.snapshot.params['id'];

    this.employee =new Employee();
    this.apiservice.getEmployeeById(this.id).subscribe(data => {
      this.employee=data;
    })
  }
  backToEmployeeList(){
    // this.router.na(['/EmployeeListComponent']);
    this.router.navigate(['/employee-list']);
  }


}
