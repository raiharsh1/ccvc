import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Employee } from './employee';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {
  [x: string]: any;
  private url="http://localhost:8080/employees"
  // httpClient: any;
  

  constructor(private httpClient:HttpClient) {}


  getemployees()
  {
    return this.httpClient.get(this.url);
  }
  getEmployeeById(id:number): Observable<Employee>{
    return this.httpClient.get<Employee>(`${this.url}/${id}`);
  }

 creatEmployee(employee:Employee): Observable<Object>{
    return this.httpClient.post<Object>(`${this.url}`,employee);

 }
 deleteEmployee(id:number):Observable<Object>{
  return this.httpClient.delete(`${this.url}/${id}`);
 }

 updateEmployee(id:number,employee: Employee): Observable<Object>{
  return this.httpClient.put(`${this.url}/${id}`,employee);
 }
}
