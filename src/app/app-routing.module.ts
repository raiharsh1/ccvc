import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { UpdateEmployeeComponent} from './update-employee/update-employee.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
const routes: Routes = [

  {path:'', component:EmployeeListComponent},
  {path:'employee-list', component:EmployeeListComponent},
  {path:'add-employee', component:AddEmployeeComponent},
  {path:'updateEmployee/:id',component:UpdateEmployeeComponent},
  {path:'employeeDetails/:id',component:EmployeeDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
