export class Employee {
    id: number | undefined;
    firstName : String | undefined;
    lastName: String | undefined;
    email : String | undefined;
    phoneNo: String | undefined;
}    
